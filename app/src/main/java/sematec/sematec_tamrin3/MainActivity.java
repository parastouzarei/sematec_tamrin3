package sematec.sematec_tamrin3;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    TextView result;
    EditText name;
    EditText family;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindviews();
    }

    private void bindviews() {

        result = (TextView) findViewById(R.id.result);
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);

        Button buttonshow = (Button) findViewById(R.id.show);
        Button buttonSave = (Button) findViewById(R.id.save);


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("Name", name.getText().toString());
                editor.putString("Family", family.getText().toString());
                editor.apply();
            }
        });

        buttonshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                String nametxt = preferences.getString("Name", "");
                String familytxt = preferences.getString("Family", "");
                if (nametxt.length() > 0 && familytxt.length() > 0) {
                    result.setText(nametxt + " " + familytxt);
                } else {
                    result.setText("No name" + " " + "No Family ");
                }
            }
        });
    }



}


